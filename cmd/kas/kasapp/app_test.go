package kasapp

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSplitCommand(t *testing.T) {
	tests := []struct {
		input        string
		expectedCmd  string
		expectedArgs []string
	}{
		{
			input:        "/abc",
			expectedCmd:  "/abc",
			expectedArgs: []string{},
		},
		{
			input:        "/abc arg1",
			expectedCmd:  "/abc",
			expectedArgs: []string{"arg1"},
		},
		{
			input:        "/abc arg1 arg2",
			expectedCmd:  "/abc",
			expectedArgs: []string{"arg1", "arg2"},
		},
	}
	for _, tc := range tests {
		t.Run(tc.input, func(t *testing.T) {
			cmd, args := splitCommand(tc.input)
			assert.Equal(t, tc.expectedCmd, cmd)
			assert.Equal(t, tc.expectedArgs, args)
		})
	}
}
