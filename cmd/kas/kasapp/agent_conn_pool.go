package kasapp

import (
	"context"

	agentk2kas_router "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/agentk2kas_tunnel/router"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type agentConnPool struct {
	internalServerConn grpc.ClientConnInterface
}

func newAgentConnPool(internalServerConn grpc.ClientConnInterface) *agentConnPool {
	return &agentConnPool{
		internalServerConn: internalServerConn,
	}
}

func (p *agentConnPool) Get(agentID int64) grpc.ClientConnInterface {
	return &internalServerConnWrapper{
		delegate: p.internalServerConn,
		agentID:  agentID,
	}
}

var (
	_ grpc.ClientConnInterface = (*internalServerConnWrapper)(nil)
)

type internalServerConnWrapper struct {
	delegate grpc.ClientConnInterface
	agentID  int64
}

func (w *internalServerConnWrapper) Invoke(ctx context.Context, method string, args any, reply any, opts ...grpc.CallOption) error {
	return w.delegate.Invoke(w.setRoutingMetadata(ctx), method, args, reply, opts...)
}

func (w *internalServerConnWrapper) NewStream(ctx context.Context, desc *grpc.StreamDesc, method string, opts ...grpc.CallOption) (grpc.ClientStream, error) {
	return w.delegate.NewStream(w.setRoutingMetadata(ctx), desc, method, opts...)
}

func (w *internalServerConnWrapper) setRoutingMetadata(ctx context.Context) context.Context {
	md, _ := metadata.FromOutgoingContext(ctx)
	md = agentk2kas_router.SetRoutingMetadata(md, w.agentID)
	return metadata.NewOutgoingContext(ctx, md)
}
