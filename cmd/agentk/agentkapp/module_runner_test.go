package agentkapp

import (
	"context"
	"testing"
	"time"

	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/agent_configuration/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/modagent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_modagent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/testing/mock_rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/pkg/agentcfg"
	"go.uber.org/mock/gomock"
	"go.uber.org/zap/zaptest"
	"golang.org/x/sync/errgroup"
	"google.golang.org/protobuf/testing/protocmp"
	"k8s.io/apimachinery/pkg/util/wait"
)

const (
	revision1 = "rev12341234_1"
	revision2 = "rev12341234_2"
)

var (
	projectID = "some/project"
)

func TestConfigurationIsApplied(t *testing.T) {
	cfg1 := &agentcfg.AgentConfiguration{}
	cfg2 := &agentcfg.AgentConfiguration{
		Gitops: &agentcfg.GitopsCF{
			ManifestProjects: []*agentcfg.ManifestProjectCF{
				{
					Id: &projectID,
				},
			},
		},
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ctrl := gomock.NewController(t)
	watcher := mock_rpc.NewMockConfigurationWatcherInterface(ctrl)
	m := mock_modagent.NewMockModule(ctrl)
	ctx1, cancel1 := context.WithCancel(context.Background())
	defer cancel1()
	ctx2, cancel2 := context.WithCancel(context.Background())
	defer cancel2()
	m.EXPECT().
		Run(gomock.Any(), gomock.Any()).
		Do(func(ctx context.Context, cfg <-chan *agentcfg.AgentConfiguration) error {
			c := <-cfg
			cancel1()
			assert.Empty(t, cmp.Diff(c, cfg1, protocmp.Transform()))
			c = <-cfg
			cancel2()
			assert.Empty(t, cmp.Diff(c, cfg2, protocmp.Transform()))
			<-ctx.Done()
			return nil
		})
	gomock.InOrder(
		watcher.EXPECT().
			Watch(gomock.Any(), gomock.Any()).
			Do(func(ctx context.Context, callback rpc.ConfigurationCallback) {
				callback(ctx, rpc.ConfigurationData{CommitID: revision1, Config: cfg1})
				<-ctx1.Done()
				callback(ctx, rpc.ConfigurationData{CommitID: revision2, Config: cfg2})
				<-ctx2.Done()
				cancel()
			}),
		m.EXPECT().
			DefaultAndValidateConfiguration(cfg1),
		m.EXPECT().
			DefaultAndValidateConfiguration(cfg2),
	)
	a := moduleRunner{
		log:                  zaptest.NewLogger(t),
		configurationWatcher: watcher,
	}
	run := a.RegisterModules([]modagent.Module{m})
	g, ctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		return run(ctx)
	})
	g.Go(func() error {
		return a.RunConfigurationRefresh(ctx)
	})
	err := g.Wait()
	require.NoError(t, err)
}

func TestConfigurationIsSquashed(t *testing.T) {
	cfg1 := &agentcfg.AgentConfiguration{}
	cfg2 := &agentcfg.AgentConfiguration{
		Gitops: &agentcfg.GitopsCF{
			ManifestProjects: []*agentcfg.ManifestProjectCF{
				{
					Id: &projectID,
				},
				{
					DefaultNamespace: "abc",
				},
			},
		},
	}
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	ctrl := gomock.NewController(t)
	watcher := mock_rpc.NewMockConfigurationWatcherInterface(ctrl)
	m := mock_modagent.NewMockModule(ctrl)
	ctx1, cancel1 := context.WithCancel(context.Background())
	defer cancel1()
	m.EXPECT().
		Run(gomock.Any(), gomock.Any()).
		Do(func(ctx context.Context, cfg <-chan *agentcfg.AgentConfiguration) error {
			<-ctx1.Done()
			c := <-cfg
			cancel()
			assert.Empty(t, cmp.Diff(c, cfg2, protocmp.Transform()))
			<-ctx.Done()
			return nil
		})
	gomock.InOrder(
		watcher.EXPECT().
			Watch(gomock.Any(), gomock.Any()).
			Do(func(ctx context.Context, callback rpc.ConfigurationCallback) {
				callback(ctx, rpc.ConfigurationData{CommitID: revision1, Config: cfg1})
				callback(ctx, rpc.ConfigurationData{CommitID: revision2, Config: cfg2})
				cancel1()
				<-ctx.Done()
			}),
		m.EXPECT().
			DefaultAndValidateConfiguration(cfg1),
		m.EXPECT().
			DefaultAndValidateConfiguration(cfg2),
	)
	a := moduleRunner{
		log:                  zaptest.NewLogger(t),
		configurationWatcher: watcher,
	}
	run := a.RegisterModules([]modagent.Module{m})
	g, ctx := errgroup.WithContext(ctx)
	g.Go(func() error {
		return run(ctx)
	})
	g.Go(func() error {
		return a.RunConfigurationRefresh(ctx)
	})
	err := g.Wait()
	require.NoError(t, err)
}

func TestModule_ModuleTerminatesWithoutStop(t *testing.T) {
	// NOTE: we cannot use the module runner to test this behavior, because the panic happens in a goroutine that we cannot recover from or assert.
	ctrl := gomock.NewController(t)
	m := mock_modagent.NewMockModule(ctrl)
	m.EXPECT().
		Run(gomock.Any(), gomock.Any()).
		DoAndReturn(func(ctxm context.Context, cfgm <-chan *agentcfg.AgentConfiguration) error {
			return nil
		})
	m.EXPECT().Name().Return("test-module")

	a := moduleHolder{
		module:      m,
		cfg2pipe:    make(chan *agentcfg.AgentConfiguration),
		pipe2module: make(chan *agentcfg.AgentConfiguration),
	}
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	assert.PanicsWithError(t, "the module 'test-module' terminated from its Run method without a stop. This is a programming error in that module. Please open an issue in https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues", func() {
		a.runModule(ctx)
	})
}

func TestModule_ModuleTerminatesDueToConfigChannelClosed(t *testing.T) {
	ctrl := gomock.NewController(t)
	m := mock_modagent.NewMockModule(ctrl)
	m.EXPECT().
		Run(gomock.Any(), gomock.Any()).
		DoAndReturn(func(ctxm context.Context, cfgm <-chan *agentcfg.AgentConfiguration) error {
			<-cfgm // wait for config channel, not for context
			return nil
		})
	m.EXPECT().Name().Return("test-module").AnyTimes()
	a := moduleHolder{
		module:      m,
		cfg2pipe:    make(chan *agentcfg.AgentConfiguration),
		pipe2module: make(chan *agentcfg.AgentConfiguration),
	}

	ctxPipe, cancelPipe := context.WithCancel(context.Background())
	ctxModule, cancelModule := context.WithCancel(context.Background())

	var wg wait.Group

	wg.Start(func() {
		assert.NoError(t, a.runPipe(ctxPipe))
	})
	wg.Start(func() {
		assert.NoError(t, a.runModule(ctxModule))
	})
	cancelPipe()
	time.Sleep(50 * time.Millisecond)
	cancelModule()
	wg.Wait()
}
