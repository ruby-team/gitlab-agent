package rpc

import "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/grpctool"

var (
	GatewayResponseVisitor = grpctool.NewLazyStreamVisitor(&GatewayResponse{})
	ConnectRequestVisitor  = grpctool.NewLazyStreamVisitor(&ConnectRequest{})
	ConnectResponseVisitor = grpctool.NewLazyStreamVisitor(&ConnectResponse{})
)
