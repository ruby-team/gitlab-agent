package agent

import (
	"context"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/agentk2kas_tunnel"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tunnel/tunclient"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/pkg/agentcfg"
	"google.golang.org/grpc"
)

type module struct {
	server *grpc.Server
	// minIdleConnections is the minimum number of connections that are not streaming a request.
	minIdleConnections int32
	// maxConnections is the maximum number of connections (idle and active).
	maxConnections int32
	scaleUpStep    int32
	// maxIdleTime is the maximum duration of time a connection can stay in an idle state.
	maxIdleTime       time.Duration
	connectionFactory tunclient.ConnectionFactory
}

func (m *module) Run(ctx context.Context, cfg <-chan *agentcfg.AgentConfiguration) error {
	cm := tunclient.NewConnectionManager(
		m.minIdleConnections,
		m.maxConnections,
		m.scaleUpStep,
		m.maxIdleTime,
		tunclient.APIDescriptor(m.server),
		m.connectionFactory,
	)
	cm.Run(ctx)
	return nil
}

func (m *module) DefaultAndValidateConfiguration(config *agentcfg.AgentConfiguration) error {
	return nil
}

func (m *module) Name() string {
	return agentk2kas_tunnel.ModuleName
}
