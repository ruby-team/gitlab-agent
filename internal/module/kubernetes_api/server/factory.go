package server

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/binary"
	"fmt"
	"net"

	gapi "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/gitlab/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/kubernetes_api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/kubernetes_api/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/cache"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/nettool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/prototool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/redistool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/tlstool"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
)

const (
	k8sAPIRequestCountKnownMetric = "k8s_api_proxy_request"
	// `ci_access` metric names
	k8sAPIProxyRequestsViaCIAccessMetricName             = "k8s_api_proxy_requests_via_ci_access"
	k8sAPIProxyRequestsUniqueAgentsViaCIAccessMetricName = "k8s_api_proxy_requests_unique_agents_via_ci_access"
	// `user_access` metric names
	k8sAPIProxyRequestsViaUserAccessMetricName             = "k8s_api_proxy_requests_via_user_access"
	k8sAPIProxyRequestsUniqueAgentsViaUserAccessMetricName = "k8s_api_proxy_requests_unique_agents_via_user_access"
	// PAT access metric names
	k8sAPIProxyRequestsViaPatAccessMetricName             = "k8s_api_proxy_requests_via_pat_access"
	k8sAPIProxyRequestsUniqueAgentsViaPatAccessMetricName = "k8s_api_proxy_requests_unique_agents_via_pat_access"
	// Event names
	k8sAPIProxyRequestsUniqueUsersViaCIAccessEventName   = "k8s_api_proxy_requests_unique_users_via_ci_access"
	k8sAPIProxyRequestsUniqueUsersViaUserAccessEventName = "k8s_api_proxy_requests_unique_users_via_user_access"
	k8sAPIProxyRequestsUniqueUsersViaPatAccessEventName  = "k8s_api_proxy_requests_unique_users_via_pat_access"
)

type Factory struct {
}

func (f *Factory) New(config *modserver.Config) (modserver.Module, error) {
	k8sAPI := config.Config.Agent.KubernetesApi
	if k8sAPI == nil {
		return &modserver.NopModule{
			ModuleName: kubernetes_api.ModuleName,
		}, nil
	}
	listenCfg := k8sAPI.Listen
	certFile := listenCfg.CertificateFile
	keyFile := listenCfg.KeyFile
	var listener func() (net.Listener, error)

	tlsConfig, err := tlstool.MaybeDefaultServerTLSConfig(certFile, keyFile)
	if err != nil {
		return nil, err
	}
	if tlsConfig != nil {
		listener = func() (net.Listener, error) {
			return nettool.TLSListenWithOSTCPKeepAlive(*listenCfg.Network, listenCfg.Address, tlsConfig)
		}
	} else {
		listener = func() (net.Listener, error) {
			return nettool.ListenWithOSTCPKeepAlive(*listenCfg.Network, listenCfg.Address)
		}
	}
	serverName := fmt.Sprintf("%s/%s/%s", config.KASName, config.Version, config.CommitID)
	var allowedOriginURLs []string
	if u := config.Config.Gitlab.GetExternalUrl(); u != "" {
		allowedOriginURLs = append(allowedOriginURLs, u)
	}
	allowedAgentCacheTTL := k8sAPI.AllowedAgentCacheTtl.AsDuration()
	allowedAgentCacheErrorTTL := k8sAPI.AllowedAgentCacheErrorTtl.AsDuration()
	tracer := config.TraceProvider.Tracer(kubernetes_api.ModuleName)
	m := &module{
		log: config.Log,
		proxy: kubernetesAPIProxy{
			log: config.Log,
			api: config.API,
			agentConnPool: func(agentID int64) rpc.KubernetesApiClient {
				return rpc.NewKubernetesApiClient(config.AgentConnPool(agentID))
			},
			gitLabClient:      config.GitLabClient,
			allowedOriginURLs: allowedOriginURLs,
			allowedAgentsCache: cache.NewWithError[string, *gapi.AllowedAgentsForJob](
				allowedAgentCacheTTL,
				allowedAgentCacheErrorTTL,
				&redistool.ErrCacher[string]{
					Log:          config.Log,
					ErrRep:       modshared.APIToErrReporter(config.API),
					Client:       config.RedisClient,
					ErrMarshaler: prototool.ProtoErrMarshaler{},
					KeyToRedisKey: func(jobToken string) string {
						// Hash half of the token. Even if that hash leaks, it's not a big deal.
						// We do the same in api.AgentToken2key().
						n := len(jobToken) / 2
						tokenHash := sha256.Sum256([]byte(jobToken[:n]))
						tokenHashStr := base64.StdEncoding.EncodeToString(tokenHash[:])
						return config.Config.Redis.KeyPrefix + ":allowed_agents_errs:" + tokenHashStr
					},
				},
				tracer,
				gapi.IsCacheableError,
			),
			authorizeProxyUserCache: cache.NewWithError[proxyUserCacheKey, *gapi.AuthorizeProxyUserResponse](
				allowedAgentCacheTTL,
				allowedAgentCacheErrorTTL,
				&redistool.ErrCacher[proxyUserCacheKey]{
					Log:           config.Log,
					ErrRep:        modshared.APIToErrReporter(config.API),
					Client:        config.RedisClient,
					ErrMarshaler:  prototool.ProtoErrMarshaler{},
					KeyToRedisKey: getAuthorizedProxyUserCacheKey(config.Config.Redis.KeyPrefix),
				},
				tracer,
				gapi.IsCacheableError,
			),
			requestCounter:           config.UsageTracker.RegisterCounter(k8sAPIRequestCountKnownMetric),
			ciAccessRequestCounter:   config.UsageTracker.RegisterCounter(k8sAPIProxyRequestsViaCIAccessMetricName),
			ciAccessAgentsCounter:    config.UsageTracker.RegisterUniqueCounter(k8sAPIProxyRequestsUniqueAgentsViaCIAccessMetricName),
			ciAccessEventTracker:     config.EventTracker.RegisterEvent(k8sAPIProxyRequestsUniqueUsersViaCIAccessEventName),
			userAccessRequestCounter: config.UsageTracker.RegisterCounter(k8sAPIProxyRequestsViaUserAccessMetricName),
			userAccessAgentsCounter:  config.UsageTracker.RegisterUniqueCounter(k8sAPIProxyRequestsUniqueAgentsViaUserAccessMetricName),
			userAccessEventTracker:   config.EventTracker.RegisterEvent(k8sAPIProxyRequestsUniqueUsersViaUserAccessEventName),
			patAccessRequestCounter:  config.UsageTracker.RegisterCounter(k8sAPIProxyRequestsViaPatAccessMetricName),
			patAccessAgentsCounter:   config.UsageTracker.RegisterUniqueCounter(k8sAPIProxyRequestsUniqueAgentsViaPatAccessMetricName),
			patAccessEventTracker:    config.EventTracker.RegisterEvent(k8sAPIProxyRequestsUniqueUsersViaPatAccessEventName),
			responseSerializer:       serializer.NewCodecFactory(runtime.NewScheme()),
			traceProvider:            config.TraceProvider,
			tracePropagator:          config.TracePropagator,
			meterProvider:            config.MeterProvider,
			serverName:               serverName,
			serverVia:                "gRPC/1.0 " + serverName,
			urlPathPrefix:            k8sAPI.UrlPathPrefix,
			listenerGracePeriod:      listenCfg.ListenGracePeriod.AsDuration(),
			shutdownGracePeriod:      listenCfg.ShutdownGracePeriod.AsDuration(),
		},
		listener: listener,
	}
	config.RegisterAgentAPI(&rpc.KubernetesApi_ServiceDesc)
	return m, nil
}

func (f *Factory) Name() string {
	return kubernetes_api.ModuleName
}

func (f *Factory) StartStopPhase() modshared.ModuleStartStopPhase {
	// Start after servers because proxy uses agent connection (config.AgentConnPool), which works by accessing
	// in-memory private API server. So proxy needs to start after and stop before that server.
	return modshared.ModuleStartAfterServers
}

func getAuthorizedProxyUserCacheKey(redisKeyPrefix string) redistool.KeyToRedisKey[proxyUserCacheKey] {
	return func(key proxyUserCacheKey) string {
		// Hash half of the token. Even if that hash leaks, it's not a big deal.
		// We do the same in api.AgentToken2key().
		n := len(key.accessKey) / 2

		// Use delimiters between fields to ensure hash of "ab" + "c" is different from "a" + "bc".
		h := sha256.New()
		id := make([]byte, 8)
		binary.LittleEndian.PutUint64(id, uint64(key.agentID))
		h.Write(id)
		// Don't need a delimiter here because id is fixed size in bytes
		h.Write([]byte(key.accessType))
		h.Write([]byte{11}) // delimiter
		h.Write([]byte(key.accessKey[:n]))
		h.Write([]byte{11}) // delimiter
		h.Write([]byte(key.csrfToken))
		tokenHash := h.Sum(nil)
		tokenHashStr := base64.StdEncoding.EncodeToString(tokenHash)
		return redisKeyPrefix + ":auth_proxy_user_errs:" + tokenHashStr
	}
}
