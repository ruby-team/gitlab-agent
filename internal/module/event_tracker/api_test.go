package event_tracker //nolint:stylecheck

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestEventTracker(t *testing.T) {
	et := NewEventTracker()
	e := et.RegisterEvent("x")
	require.Contains(t, et.eventLists, "x")

	ed := et.CloneEventData()
	expectedEvents := map[string][]Event{}
	require.Equal(t, expectedEvents, ed.Events)

	e.EmitEvent(1, 2)
	e.EmitEvent(4, 5)
	e.EmitEvent(1, 2)
	assert.EqualValues(t, 3, et.AccumulatedEvents())

	ed = et.CloneEventData()
	expectedEvents = map[string][]Event{
		"x": {
			{
				UserID:    1,
				ProjectID: 2,
			},
			{
				UserID:    4,
				ProjectID: 5,
			},
			{
				UserID:    1,
				ProjectID: 2,
			},
		},
	}
	require.Equal(t, expectedEvents, ed.Events)

	et.Subtract(ed)
	ed = et.CloneEventData()
	require.Empty(t, ed.Events)
	assert.Zero(t, et.AccumulatedEvents())
}

func TestEventTracker_Subtract(t *testing.T) {
	et := NewEventTracker()
	e := et.RegisterEvent("x")
	require.Contains(t, et.eventLists, "x")

	e.EmitEvent(1, 1)
	e.EmitEvent(2, 2)
	e.EmitEvent(3, 3)
	e.EmitEvent(4, 4)
	e.EmitEvent(5, 5)
	require.Len(t, et.eventLists["x"].events, 5)
	assert.EqualValues(t, 5, et.AccumulatedEvents())

	et.Subtract(&EventData{
		Events: map[string][]Event{
			"x": {
				{
					UserID:    1,
					ProjectID: 1,
				},
				{
					UserID:    2,
					ProjectID: 2,
				},
				{
					UserID:    3,
					ProjectID: 3,
				},
			},
		},
	})
	require.Len(t, et.eventLists["x"].events, 2)
	assert.EqualValues(t, 2, et.AccumulatedEvents())

	e.EmitEvent(6, 6)
	assert.EqualValues(t, 3, et.AccumulatedEvents())

	et.Subtract(&EventData{
		Events: map[string][]Event{
			"x": {
				{
					UserID:    4,
					ProjectID: 4,
				},
				{
					UserID:    5,
					ProjectID: 5,
				},
				{
					UserID:    6,
					ProjectID: 6,
				},
			},
		},
	})
	require.Empty(t, et.eventLists["x"].events)
	assert.Zero(t, et.AccumulatedEvents())
}
