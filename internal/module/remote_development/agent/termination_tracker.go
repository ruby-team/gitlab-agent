package agent

// terminationTrackerKey is used as a key within terminationTracker
// to uniquely identify a combination of workspace name and namespace that must be tracked
type terminationTrackerKey struct {
	name      string
	namespace string
}

type terminationDetails struct {
	progress TerminationProgress

	// isSynced indicates whether progress has been synced with Rails or not
	isSynced bool
}

// terminationTracker is a set to track workspaces that exist in Terminating/Terminated state
type terminationTracker map[terminationTrackerKey]terminationDetails

func newTerminationTracker() terminationTracker {
	return make(map[terminationTrackerKey]terminationDetails)
}

func (t terminationTracker) set(name string, namespace string, details terminationDetails) {
	key := terminationTrackerKey{
		name:      name,
		namespace: namespace,
	}
	t[key] = details
}

func (t terminationTracker) delete(name string, namespace string) {
	key := terminationTrackerKey{
		name:      name,
		namespace: namespace,
	}
	delete(t, key)
}
