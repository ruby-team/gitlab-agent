package mock_tunnel_tunserver

// This package imports internal/tunnel/server, so it cannot be used in tests in that package
// because of circular imports. Some of the same mocks are generated locally in that package.

//go:generate mockgen.sh -destination "tunnel.go" -package "mock_tunnel_tunserver" "gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tunnel/tunserver" "DataCallback"
