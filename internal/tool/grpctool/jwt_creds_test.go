package grpctool

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/tool/grpctool/test"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"k8s.io/apimachinery/pkg/util/wait"
)

var (
	_ credentials.PerRPCCredentials = &JWTCredentials{}
)

const (
	secret   = "dfjnfkadskfadsnfkjadsgkasdbg"
	audience = "fasfadsf"
	issuer   = "cbcxvbvxbxb"
)

func TestJWTCredentialsProducesValidToken(t *testing.T) {
	c := &JWTCredentials{
		Secret:   []byte(secret),
		Audience: audience,
		Issuer:   issuer,
		Insecure: true,
	}
	auther := NewJWTAuther([]byte(secret), issuer, audience, func(ctx context.Context) *zap.Logger { //nolint: misspell
		return zaptest.NewLogger(t)
	})
	listener := NewDialListener()

	srv := grpc.NewServer(
		grpc.ChainStreamInterceptor(
			auther.StreamServerInterceptor,
		),
		grpc.ChainUnaryInterceptor(
			auther.UnaryServerInterceptor,
		),
	)
	test.RegisterTestingServer(srv, &test.GRPCTestingServer{
		UnaryFunc: func(ctx context.Context, request *test.Request) (*test.Response, error) {
			return &test.Response{
				Message: &test.Response_Scalar{Scalar: 123},
			}, nil
		},
		StreamingFunc: func(server test.Testing_StreamingRequestResponseServer) error {
			return server.Send(&test.Response{
				Message: &test.Response_Scalar{Scalar: 123},
			})
		},
	})
	var wg wait.Group
	defer wg.Wait()
	defer srv.GracefulStop()
	wg.Start(func() {
		assert.NoError(t, srv.Serve(listener))
	})
	conn, err := grpc.NewClient("passthrough:pipe",
		grpc.WithContextDialer(listener.DialContext),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithPerRPCCredentials(c),
	)
	require.NoError(t, err)
	defer conn.Close()
	client := test.NewTestingClient(conn)
	_, err = client.RequestResponse(context.Background(), &test.Request{})
	require.NoError(t, err)
	stream, err := client.StreamingRequestResponse(context.Background())
	require.NoError(t, err)
	_, err = stream.Recv()
	require.NoError(t, err)
}
