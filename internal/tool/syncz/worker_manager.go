package syncz

import (
	"context"
	"fmt"

	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/util/wait"
)

type WorkSource[ID comparable, C any] struct {
	ID            ID
	Configuration C
}

type WorkerFactory[ID comparable, C any] interface {
	New(WorkSource[ID, C]) Worker
}

type WorkerManager[ID comparable, C any] struct {
	log           *zap.Logger
	fld           func(ID) zap.Field
	workerFactory WorkerFactory[ID, C]
	equal         func(c1, c2 C) bool
	workers       map[ID]*workerHolder[ID, C] // source id -> worker holder instance
}

func NewWorkerManager[ID comparable, C any](log *zap.Logger, fld func(ID) zap.Field, workerFactory WorkerFactory[ID, C], equal func(c1, c2 C) bool) *WorkerManager[ID, C] {
	return &WorkerManager[ID, C]{
		log:           log,
		fld:           fld,
		workerFactory: workerFactory,
		equal:         equal,
		workers:       map[ID]*workerHolder[ID, C]{},
	}
}

func (m *WorkerManager[ID, C]) startNewWorker(source WorkSource[ID, C]) {
	m.log.Info("Starting worker", m.fld(source.ID))
	worker := m.workerFactory.New(source)
	ctx, cancel := context.WithCancel(context.Background())
	holder := &workerHolder[ID, C]{
		src:  source,
		stop: cancel,
	}
	holder.wg.StartWithContext(ctx, worker.Run)
	m.workers[source.ID] = holder
}

func (m *WorkerManager[ID, C]) ApplyConfiguration(sources []WorkSource[ID, C]) error {
	newSetOfSources := make(map[ID]struct{}, len(sources))
	var sourcesToStartWorkersFor []WorkSource[ID, C]
	var workersToStop []*workerHolder[ID, C] //nolint:prealloc

	// Collect sources without workers or with updated configuration.
	for _, source := range sources {
		id := source.ID
		if _, ok := newSetOfSources[id]; ok {
			return fmt.Errorf("duplicate source id: %v", id)
		}
		newSetOfSources[id] = struct{}{}
		holder := m.workers[id]
		if holder == nil { // New source added
			sourcesToStartWorkersFor = append(sourcesToStartWorkersFor, source)
		} else { // We have a worker for this source already
			if m.equal(source.Configuration, holder.src.Configuration) {
				// Worker's configuration hasn't changed, nothing to do here
				continue
			}
			m.log.Info("Configuration changed, restarting worker", m.fld(id))
			workersToStop = append(workersToStop, holder)
			sourcesToStartWorkersFor = append(sourcesToStartWorkersFor, source)
		}
	}

	// Stop workers for sources which have been removed from the list.
	for sourceID, holder := range m.workers {
		if _, ok := newSetOfSources[sourceID]; ok {
			continue
		}
		workersToStop = append(workersToStop, holder)
	}

	// Tell workers that should be stopped to stop.
	for _, holder := range workersToStop {
		m.log.Info("Stopping worker", m.fld(holder.src.ID))
		holder.stop()
		delete(m.workers, holder.src.ID)
	}

	// Wait for stopped workers to finish.
	for _, holder := range workersToStop {
		m.log.Info("Waiting for worker to stop", m.fld(holder.src.ID))
		holder.wg.Wait()
		m.log.Info("Stopped worker", m.fld(holder.src.ID))
	}

	// Start new workers for new sources or because of updated configuration.
	for _, source := range sourcesToStartWorkersFor {
		m.startNewWorker(source)
	}
	return nil
}

func (m *WorkerManager[ID, C]) StopAllWorkers() {
	// Tell all workers to stop
	for _, holder := range m.workers {
		m.log.Info("Stopping worker", m.fld(holder.src.ID))
		holder.stop()
	}
	// Wait for all workers to stop
	for k, holder := range m.workers {
		m.log.Info("Waiting for worker to stop", m.fld(holder.src.ID))
		holder.wg.Wait()
		// we clear the workers map to allow for a proper restart of the workers.
		delete(m.workers, k)
		m.log.Info("Stopped worker", m.fld(holder.src.ID))
	}
}

type workerHolder[ID comparable, C any] struct {
	src  WorkSource[ID, C]
	wg   wait.Group
	stop context.CancelFunc
}
