package api

import (
	"context"
	"net/url"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/gitlab"
)

const (
	ProjectInfoAPIPath  = "/api/v4/internal/kubernetes/project_info"
	ProjectIDQueryParam = "id"
)

func GetProjectInfo(ctx context.Context, client gitlab.ClientInterface, agentToken api.AgentToken, projectID string, opts ...gitlab.DoOption) (*api.ProjectInfo, error) {
	response := &GetProjectInfoResponse{}
	err := client.Do(ctx,
		joinOpts(opts,
			gitlab.WithPath(ProjectInfoAPIPath),
			gitlab.WithQuery(url.Values{
				ProjectIDQueryParam: []string{projectID},
			}),
			gitlab.WithAgentToken(agentToken),
			gitlab.WithResponseHandler(gitlab.ProtoJSONResponseHandler(response)),
			gitlab.WithJWT(true),
		)...,
	)
	if err != nil {
		return nil, err
	}
	return response.ToAPIProjectInfo(), nil
}
