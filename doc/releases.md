# Release Process

> **Attention**
>
> When preparing for the next KAS milestone release, it is crucial to confirm that the GitLab stable release branch
> for the current milestone release has already been created in the [GitLab](https://gitlab.com/gitlab-org/gitlab) repository.
> You can identify this branch by its format, which should look like this: `<major>-<minor>-stable-ee`. For instance, `16-9-stable-ee`.
> This is important to prevent any `X.Y+1` KAS release from mistakenly being included in an `X.Y` GitLab release.
> For example, we wouldn't want a `16.10.0` KAS release to appear in a GitLab `16.9.0` release.
>
> Here is an example to illustrate this process:
> - Let's say the current milestone is 16.9.
> - A `gitlab-agent` 16.9 release has already been created and deployed.
> - Wait for the creation of the `16-9-stable-ee` branch in the [GitLab](https://gitlab.com/gitlab-org/gitlab) repository.
> - Only after this branch has been created should you proceed to create any `16.10.Z(-rcN)` release.

1. On the 15th, an automatic Slack notification reminds the Environments team to create a monthly release. Actual release date can be found on the [GitLab releases](https://about.gitlab.com/releases/) page.
1. On the 15th, tag a new version that matches the upcoming GitLab minor version. E.g. If the upcoming milestone is 13.7,
   then tag `v13.7.0`.
1. **GitLab KAS** for GitLab distributions (CNG and Omnibus)
    1. *(Automated)* An MR to update [`GITLAB_KAS_VERSION`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/GITLAB_KAS_VERSION)
      file to the new tag in the GitLab repository is raised.
      [Example MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/111845).
      [List of bot-created MRs](https://gitlab.com/gitlab-org/gitlab/-/merge_requests?scope=all&state=all&label_name[]=group%3A%3Aenvironments&author_username=gitlab-dependency-update-bot).
      [Bot configuration](https://gitlab.com/gitlab-org/frontend/renovate-gitlab-bot/-/blob/main/renovate/gitlab/version-files.config.js).
      [Dependency dashboard](https://gitlab.com/gitlab-org/gitlab/-/issues/390663).
    1. The MR is merged by someone from the
      [KAS Version Maintainers](https://gitlab.com/groups/gitlab-org/maintainers/kas-version-maintainers/-/group_members?with_inherited_permissions=exclude)
      group.
    1. (Automated) kas is built by [CNG](https://gitlab.com/gitlab-org/build/CNG) and deployed to `gstg` and `gprd`.
    1. On the release date, the `GITLAB_KAS_VERSION` is automatically synced for when release team tags new Omnibus and chart releases.
1. **GitLab Agent Helm Chart**
    1. *(Automated)* An MR to update the `appVersion` in the [GitLab Agent Helm Chart](https://gitlab.com/gitlab-org/charts/gitlab-agent#publishing-a-new-release) is created.
    1. The MR is merged by one of the GitLab Agent Helm Chart maintainers.
    1. Once the MR is merged:
      - Bump `version` according to semantic versioning. For a GitLab milestone release, this will generally be the minor version.
      - Push a tag `vX.Y.Z` where `X.Y.Z` is the `version` in `Chart.yaml`.

## Troubleshooting

### Image with desired tag is not on `dev.gitlab.org`

The image deployed to `dev.gitlab.org` is built within the pipeline of the
[security images project](https://gitlab.com/gitlab-org/security/charts/components/images) in a 3 hour frequency.
The version to built is taken from the [`GITLAB_KAS_VERSION`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/GITLAB_KAS_VERSION).

### Image Pull Error in gstg / dev cluster

This may happen because the image from `dev.gitlab.org` is not yet synced with the high availability registry,
where the KAS image is eventually pulled from. This registry may lag 1.5-2 hours behind.
This sync happens in the `sync-images-artifact-registry` job of the pipeline in the
[security images project](https://gitlab.com/gitlab-org/security/charts/components/images).
