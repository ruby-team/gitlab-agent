## 16.11.5 (2024-06-25)

No changes.

## 16.11.4 (2024-06-11)

### Security (1 change)

- [Ensure required AgentMeta is present or handled when missing](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/commit/0fd9a7d1576246b7e5eb97ec4461c23dafe37051) ([merge request](https://gitlab.com/gitlab-org/security/cluster-integration/gitlab-agent/-/merge_requests/11))

## 16.11.3 (2024-05-21)

No changes.

## 16.11.2 (2024-05-07)

### Fixed (1 change)

- [Do not allow to inject version from outside](gitlab-org/security/cluster-integration/gitlab-agent@141707b32694692d495979eb3191004de3964962)

### Changed (1 change)

- [Increase max size of Agent Meta Version string](gitlab-org/security/cluster-integration/gitlab-agent@ebe741712fd4b178668c3485e1bd99cc041a14f9)

### Other (1 change)

- [Introduce support for VERSION file](gitlab-org/security/cluster-integration/gitlab-agent@aefc4f5a3513fb0d67283ec3d11649a601977d5d)

## 16.11.0 (2024-04-17)

No changes.
